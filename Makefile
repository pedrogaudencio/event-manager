run:
	./manage.py runserver --werkzeug
normal:
	./manage.py runserver
deps:
	sudo pip install django-smart-selects

setup:
	./manage.py syncdb
	./manage.py migrate
	
deps-develop:
	sudo pip install git+git://github.com/dcramer/django-devserver#egg=django-devserver
	sudo pip install sqlparse
	sudo pip install werkzeug
	sudo pip install guppy
	sudo pip install line_profiler
