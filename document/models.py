#from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models
import os


#models, and stuff
class Document_type(models.Model):
    '''defines available document types'''
    type_name = models.CharField(max_length=100, verbose_name="type")
    extensions_allowed = models.CharField(max_length=100, verbose_name="extensions allowed")
    trashed_at = models.DateTimeField(blank=True, null=True)

    def __unicode__(self):
        return self.type_name

    def clean(self):
        #min length of text fields
        min = 2
        if len(self.type_name) <= min:
            raise ValidationError("type is too short, minimum of %d characters" % (min + 1))

    class Meta:
        verbose_name = "Document Type"
        verbose_name_plural = "Document Types"


class Document(models.Model):
    '''defines a document'''
    creator = models.ForeignKey(User, verbose_name="creator", on_delete=models.SET('DELETED'), default=User)
    edited_by = models.ForeignKey(User, verbose_name="last edited by", on_delete=models.SET('DELETED'), default=User, related_name='document_last_edited_by')
    type = models.ForeignKey(Document_type, verbose_name="type", on_delete=models.PROTECT)
    title = models.CharField(max_length=100)
    file_location = models.FileField(upload_to=settings.ACTIVITY_DOC_ROOT, max_length=500)
    creation_date = models.DateTimeField(auto_now_add=True, verbose_name="creation date")
    description = models.CharField(max_length=500, blank=True)
    trashed_at = models.DateTimeField(blank=True, null=True)

    def clean(self):
        import string
        #min length of text fields
        min = 3
        if len(self.title) <= min:
            raise ValidationError("title name is too short, minimum of %d characters" % (min + 1))
        #file extension check
        bad_chars = ',;:.-_'
        extensions = str(Document_type.objects.get(pk=self.type.id).extensions_allowed)
        #strip bad chars for file types
        extensions = extensions.translate(string.maketrans("", "", ), bad_chars)
        for e in extensions.split():
            if self.file_location.name.endswith(e):
                return
        raise ValidationError("Invalid file extension! Valid extensions for this type are %s" % extensions)

    def __unicode__(self):
        return self.title

    @property
    def supports_csv(self):
        return False

    @property
    def filename(self):
        return os.path.basename(self.file_location.name)

    class Meta:
        verbose_name = "Document"
        verbose_name_plural = "Documents"
