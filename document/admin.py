from django.contrib import admin
from document.models import Document, Document_type


class Document_admin(admin.ModelAdmin):

    list_display = ['title', 'type', 'creator', 'edited_by', 'creation_date']
    search_fields = ['title', 'creator', 'edited_by']
    exclude = ['creator', 'edited_by', 'trashed_at']
    list_filter = ['type__type_name', 'creation_date']

    def save_model(self, request, obj, form, change):
        instance = form.save(commit=False)
        if not hasattr(instance, 'creator'):
            instance.creator = request.user
        instance.edited_by = request.user
        instance.save()
        form.save_m2m()
        return instance
admin.site.register(Document, Document_admin)


class Document_type_admin(admin.ModelAdmin):

    list_display = ['type_name', 'extensions_allowed']
    search_fields = ['type_name', 'extensions_allowed']
    exclude = ['trashed_at']

    def save_model(self, request, obj, form, change):
        '''
        strips bad characters from file type
        '''
        import string
        bad_chars = ',;:.-_'
        instance = form.save(commit=False)
        ex = str(instance.extensions_allowed)
        ex = ex.replace(';', ' ')
        ex = ex.translate(string.maketrans("", "", ), bad_chars)
        instance.extensions_allowed = ex
        instance.save()
        form.save_m2m()
        return instance

admin.site.register(Document_type, Document_type_admin)