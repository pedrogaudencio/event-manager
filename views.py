# Create your views here.
## -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView
from django.http import HttpResponse, Http404
from django.views.generic.simple import direct_to_template
import settings
from event.models import Activity, UserParticipatesInActivity, \
    UserParticipatesInActivityMetadata
from django.contrib.auth.models import User


def viewparticipants(request, app_label, model, id):
    '''
    main index page
    '''
    template_name = "viewparticipants.html"
    activity = Activity.objects.get(pk=id)
    table = []
    users_in_event = UserParticipatesInActivity.objects.filter(activity=activity).values_list('user')
    users = User.objects.filter(pk__in=users_in_event)
    metadata = []
    for u in users:
        temp_metadata = []
        temp = UserParticipatesInActivityMetadata.objects.filter(user=u, activity=activity)
        for t in temp:
            temp_metadata.append([t.attribute.name, t.value])
        table.append([u, temp_metadata])
    extra_context = {'STATIC_URL': settings.STATIC_URL, 'users': table, 'activity': activity}
    return direct_to_template(request,
                              template_name,
                              extra_context=extra_context)
