from django.conf.urls.defaults import *
from django.conf import settings
from django.views.generic.simple import direct_to_template

from profiles.forms import SignupFormExtra
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
admin.autodiscover()

urlpatterns = patterns('',
    (r'^admin/doc/', include('django.contrib.admindocs.urls')),
    (r'^admin/(?P<app_label>.*)/(?P<model>.*)/(?P<id>.*)/viewparticipants/', 'aauecurriculos.views.viewparticipants'),
    (r'^admin/', include(admin.site.urls)),

    # Demo Override the signup form with our own, which includes a 
    # first and last name.
    (r'^accounts/signup/$',
      'userena.views.signup',
      {'signup_form': SignupFormExtra}),

    (r'^accounts/', include('aauecurriculos.userena.urls')),
    (r'^messages/', include('aauecurriculos.userena.contrib.umessages.urls')),
    (r'^events/', include('aauecurriculos.event.urls')),
    url(r'^$',
        direct_to_template,
        {'template': 'static/promo.html'},
        name='promo'),
    (r'^i18n/', include('django.conf.urls.i18n')),
)
if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$',
         'django.views.static.serve',
         {'document_root': settings.MEDIA_ROOT, 'show_indexes': True, }),
)
if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^static/(?P<path>.*)$',
         'django.views.static.serve',
         {'document_root': settings.STATIC_ROOT, 'show_indexes': True, }),
)
urlpatterns += staticfiles_urlpatterns()
