from event.models import Event, Activity, Attribute, ActivityHasAttributes, UserParticipatesInActivityMetadata, UserParticipatesInActivity
from django.contrib import admin, messages
from django import forms
from easy_maps.widgets import AddressWithMapWidget
from django.http import HttpResponse
#from reportlab.lib.units import inch
from django.utils.datetime_safe import datetime


admin.site.register(ActivityHasAttributes)


class UserParticipatesInActivityMetadata_admin(admin.ModelAdmin):
    list_display = ['value', 'user', 'activity', 'attribute', 'enrollment_date']
    search_fields = ['value']
    list_filter = ['enrollment_date', 'activity__name', 'attribute__type']
admin.site.register(UserParticipatesInActivityMetadata, UserParticipatesInActivityMetadata_admin)


class UserParticipatesInActivity_admin(admin.ModelAdmin):
    list_display = ['user', 'activity', 'enrollment_date', 'status']
    search_fields = ['user', 'activity__name']
    list_filter = ['enrollment_date', 'activity__name', 'status']
admin.site.register(UserParticipatesInActivity, UserParticipatesInActivity_admin)


class Attribute_admin(admin.ModelAdmin):
    list_display = ['name', 'type']
    search_fields = ['name']
    list_filter = ['type']
admin.site.register(Attribute, Attribute_admin)


class Event_admin(admin.ModelAdmin):

    list_display = ['name', 'start_date', 'end_date']
    search_fields = ['name', ]
    #exclude = ['creator','edited_by', 'trashed_at']
    list_filter = ['start_date', 'end_date']
admin.site.register(Event, Event_admin)


class Attributes_in_line(admin.TabularInline):
    model = Activity.attributes.through #@UndefinedVariable
    extra = 1


class Activity_admin(admin.ModelAdmin):
    list_display = ['name', 'start_date', 'end_date', 'event', 'view_participants_button']
    search_fields = ['name', 'event__name']
    list_filter = ['start_date', 'end_date', 'event__name']
    exclude = ['attributes']
    inlines = [Attributes_in_line]
    filter_horizontal = ['participants', 'attachments']
    actions = ['enrolled_list']

    def enrolled_list(self, request, queryset):
        '''
        return enroled list for activity in pdf
        '''
        try:
            from cStringIO import StringIO
        except ImportError:
            from StringIO import StringIO
        from reportlab.pdfgen import canvas
        from django.http import HttpResponse

        if len(queryset) != 1:
            messages.error(request, "Please select only 1 activity")
            return
        else:
            from reportlab.lib.pagesizes import letter
            #get the necessary variables
            activity = queryset[0]
            participants = activity.participants.values_list('id', 'username', 'email')
            # Create the HttpResponse object with the appropriate PDF headers.
            response = HttpResponse(mimetype='application/pdf')
            response['Content-Disposition'] = 'attachment; filename={0}.pdf'.format((activity.name + '' + unicode(datetime.now())))
            buffer = StringIO()
            # Create the PDF object, using the StringIO object as its "file."
            p = canvas.Canvas(buffer, pagesize=letter)
            textobject = p.beginText()
            textobject.setTextOrigin(50, 700)
            #p.drawCentredString(1.75 * inch, 1.5 * inch, "Font size examples")
            #textobject.setTextOrigin(3, 2.5 * inch)
            textobject.setFont("Helvetica", 20)
            textobject.textLine(activity.name)
            textobject.textLine()
            y = textobject.getY()
            textobject.setFont("Helvetica", 16)
            textobject.textLine('{0} utilizadores inscritos'.format(len(participants)))
            textobject.textLine()
            textobject.setFont("Helvetica", 11)
            for user in participants:
                textobject.textLine('Utilizador: ' + user[1])
                textobject.textLine('Email: ' + user[2])
                textobject.textLine('-------------------------------------')
            p.drawText(textobject)
            # Draw things on the PDF. Here's where the PDF generation happens.
            # See the ReportLab documentation for the full list of functionality.
            #p.drawString(100, 100, "Hello world.")

            # Close the PDF object cleanly.
            p.showPage()
            p.save()

            # Get the value of the StringIO buffer and write it to the response.
            pdf = buffer.getvalue()
            buffer.close()
            response.write(pdf)
            return response

    enrolled_list.short_description = "export enrolled list"

    def pdf_render_to_response(self, template, context, filename=None, prompt=False):
        from django.http import HttpResponse
        from django.template import loader, Context
        from trml2pdf import trml2pdf
        response = HttpResponse(mimetype='application/pdf')
        if not filename:
            filename = template + '.pdf'
        cd = []
        if prompt:
            cd.append('attachment')
        cd.append('filename=%s' % filename)
        response['Content-Disposition'] = '; '.join(cd)
        tpl = loader.get_template(template)
        tc = {'filename': filename}
        tc.update(context)
        ctx = Context(tc)
        pdf = trml2pdf.parseString(tpl.render(ctx))
        response.write(pdf)
        return response

    class form(forms.ModelForm):
        class Meta:
            widgets = {
                'map_location': AddressWithMapWidget({'class': 'vTextField'})
            }
    #exclude = ['trashed_at']
admin.site.register(Activity, Activity_admin)
