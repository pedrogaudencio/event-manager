# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'Attribute'
        db.create_table('event_attribute', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=10)),
        ))
        db.send_create_signal('event', ['Attribute'])

        # Adding model 'Event'
        db.create_table('event_event', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100)),
            ('description', self.gf('django.db.models.fields.TextField')(max_length=500)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=100, null=True, blank=True)),
            ('logo', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
            ('start_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('end_date', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal('event', ['Event'])

        # Adding model 'Activity'
        db.create_table('event_activity', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('description', self.gf('django.db.models.fields.TextField')(max_length=500)),
            ('extra_info', self.gf('django.db.models.fields.TextField')(max_length=500, null=True, blank=True)),
            ('public', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('default_status', self.gf('django.db.models.fields.CharField')(default='enrolled', max_length=10)),
            ('start_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('end_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('event', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['event.Event'])),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=100, null=True, blank=True)),
            ('max_slots', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal('event', ['Activity'])

        # Adding unique constraint on 'Activity', fields ['name', 'event']
        db.create_unique('event_activity', ['name', 'event_id'])

        # Adding M2M table for field attachments on 'Activity'
        db.create_table('event_activity_attachments', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('activity', models.ForeignKey(orm['event.activity'], null=False)),
            ('document', models.ForeignKey(orm['document.document'], null=False))
        ))
        db.create_unique('event_activity_attachments', ['activity_id', 'document_id'])

        # Adding model 'ActivityHasAttributes'
        db.create_table('event_activityhasattributes', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('attribute', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['event.Attribute'])),
            ('activity', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['event.Activity'])),
            ('mandatory', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('helptext', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('extra', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal('event', ['ActivityHasAttributes'])

        # Adding unique constraint on 'ActivityHasAttributes', fields ['attribute', 'activity']
        db.create_unique('event_activityhasattributes', ['attribute_id', 'activity_id'])

        # Adding model 'UserParticipatesInActivity'
        db.create_table('event_userparticipatesinactivity', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('activity', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['event.Activity'])),
            ('enrollment_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('status', self.gf('django.db.models.fields.CharField')(max_length=10)),
        ))
        db.send_create_signal('event', ['UserParticipatesInActivity'])

        # Adding unique constraint on 'UserParticipatesInActivity', fields ['user', 'activity']
        db.create_unique('event_userparticipatesinactivity', ['user_id', 'activity_id'])

        # Adding model 'UserParticipatesInActivityMetadata'
        db.create_table('event_userparticipatesinactivitymetadata', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('activity', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['event.Activity'])),
            ('attribute', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['event.Attribute'])),
            ('enrollment_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('change_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('value', self.gf('django.db.models.fields.CharField')(max_length=1000)),
        ))
        db.send_create_signal('event', ['UserParticipatesInActivityMetadata'])

        # Adding unique constraint on 'UserParticipatesInActivityMetadata', fields ['user', 'activity', 'attribute']
        db.create_unique('event_userparticipatesinactivitymetadata', ['user_id', 'activity_id', 'attribute_id'])


    def backwards(self, orm):
        
        # Removing unique constraint on 'UserParticipatesInActivityMetadata', fields ['user', 'activity', 'attribute']
        db.delete_unique('event_userparticipatesinactivitymetadata', ['user_id', 'activity_id', 'attribute_id'])

        # Removing unique constraint on 'UserParticipatesInActivity', fields ['user', 'activity']
        db.delete_unique('event_userparticipatesinactivity', ['user_id', 'activity_id'])

        # Removing unique constraint on 'ActivityHasAttributes', fields ['attribute', 'activity']
        db.delete_unique('event_activityhasattributes', ['attribute_id', 'activity_id'])

        # Removing unique constraint on 'Activity', fields ['name', 'event']
        db.delete_unique('event_activity', ['name', 'event_id'])

        # Deleting model 'Attribute'
        db.delete_table('event_attribute')

        # Deleting model 'Event'
        db.delete_table('event_event')

        # Deleting model 'Activity'
        db.delete_table('event_activity')

        # Removing M2M table for field attachments on 'Activity'
        db.delete_table('event_activity_attachments')

        # Deleting model 'ActivityHasAttributes'
        db.delete_table('event_activityhasattributes')

        # Deleting model 'UserParticipatesInActivity'
        db.delete_table('event_userparticipatesinactivity')

        # Deleting model 'UserParticipatesInActivityMetadata'
        db.delete_table('event_userparticipatesinactivitymetadata')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'document.document': {
            'Meta': {'object_name': 'Document'},
            'creation_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'creator': ('django.db.models.fields.related.ForeignKey', [], {'default': "orm['auth.User']", 'to': "orm['auth.User']"}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '500', 'blank': 'True'}),
            'edited_by': ('django.db.models.fields.related.ForeignKey', [], {'default': "orm['auth.User']", 'related_name': "'document_last_edited_by'", 'to': "orm['auth.User']"}),
            'file_location': ('django.db.models.fields.files.FileField', [], {'max_length': '500'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'trashed_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['document.Document_type']"})
        },
        'document.document_type': {
            'Meta': {'object_name': 'Document_type'},
            'extensions_allowed': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'trashed_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'type_name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'event.activity': {
            'Meta': {'unique_together': "(('name', 'event'),)", 'object_name': 'Activity'},
            'attachments': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['document.Document']", 'null': 'True', 'blank': 'True'}),
            'attributes': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['event.Attribute']", 'null': 'True', 'through': "orm['event.ActivityHasAttributes']", 'blank': 'True'}),
            'default_status': ('django.db.models.fields.CharField', [], {'default': "'enrolled'", 'max_length': '10'}),
            'description': ('django.db.models.fields.TextField', [], {'max_length': '500'}),
            'end_date': ('django.db.models.fields.DateTimeField', [], {}),
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['event.Event']"}),
            'extra_info': ('django.db.models.fields.TextField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_slots': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'participants': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'userparticipates'", 'to': "orm['auth.User']", 'through': "orm['event.UserParticipatesInActivity']", 'blank': 'True', 'symmetrical': 'False', 'null': 'True'}),
            'participants_metadata': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'usermetadata'", 'to': "orm['auth.User']", 'through': "orm['event.UserParticipatesInActivityMetadata']", 'blank': 'True', 'symmetrical': 'False', 'null': 'True'}),
            'public': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'start_date': ('django.db.models.fields.DateTimeField', [], {}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        'event.activityhasattributes': {
            'Meta': {'unique_together': "(('attribute', 'activity'),)", 'object_name': 'ActivityHasAttributes'},
            'activity': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['event.Activity']"}),
            'attribute': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['event.Attribute']"}),
            'extra': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'helptext': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mandatory': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        'event.attribute': {
            'Meta': {'object_name': 'Attribute'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        'event.event': {
            'Meta': {'ordering': "['end_date']", 'object_name': 'Event'},
            'description': ('django.db.models.fields.TextField', [], {'max_length': '500'}),
            'end_date': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'start_date': ('django.db.models.fields.DateTimeField', [], {}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        'event.userparticipatesinactivity': {
            'Meta': {'unique_together': "(('user', 'activity'),)", 'object_name': 'UserParticipatesInActivity'},
            'activity': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['event.Activity']"}),
            'enrollment_date': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'event.userparticipatesinactivitymetadata': {
            'Meta': {'unique_together': "(('user', 'activity', 'attribute'),)", 'object_name': 'UserParticipatesInActivityMetadata'},
            'activity': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['event.Activity']"}),
            'attribute': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['event.Attribute']"}),
            'change_date': ('django.db.models.fields.DateTimeField', [], {}),
            'enrollment_date': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '1000'})
        }
    }

    complete_apps = ['event']
