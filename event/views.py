# -*- coding: utf-8 -*-
from django.views.generic import TemplateView
from django.views.generic.list import ListView
from event.models import Event, Activity, Attribute, ActivityHasAttributes, \
    UserParticipatesInActivity, UserParticipatesInActivityMetadata
from django.shortcuts import get_object_or_404, render_to_response, redirect
from django.forms.formsets import formset_factory
from event.forms import DinamicForm
from django.views.generic.simple import direct_to_template
from userena.decorators import secure_required
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.contrib import messages
from django.views.generic.detail import DetailView
from userena.utils import get_profile_model
from django.contrib.auth.models import User
import settings
from django.utils.datetime_safe import datetime


@secure_required
@login_required
def register_activity(request, event_id, activity_id, extra_context=None):
    '''
    formulario de registar numa actividade
    extra_content:
     - update = templete information that the user is already enrolled and can show different options
    '''
    #get the event, used more to stop fetching wrong activities
    event = get_object_or_404(Event, pk__iexact=event_id)
    #get the activity
    activity = get_object_or_404(Activity, pk__iexact=activity_id, event__exact=event_id)
    #set the dictionary for extra content for template engine
    if not extra_context: extra_context = dict()
    #see if the user already is already enrolled
    try:
        UserParticipatesInActivity.objects.get(user=request.user, activity=activity)
        old_attributes = UserParticipatesInActivityMetadata.objects.filter(user=request.user, activity=activity)
        extra_context['update'] = True
        old_data = {}
        for o in old_attributes:
            old_data[Attribute.objects.get(pk=o.attribute.id).name] = o.value
    except ObjectDoesNotExist:
        old_data = None
        extra_context['update'] = False
    #get the related metadata to fill
    att = ActivityHasAttributes.objects.filter(activity=activity_id)
    forms_to_fill = []
    #dicionario com objecto de attribute e boolean com obrigatoriedade em estar preenchido
    for x in att:
        a = Attribute.objects.get(pk=x.attribute_id)
        forms_to_fill.append({'name': a.name, 'type': a.type, 'extra': x.extra, 'mandatory': x.mandatory, 'help': x.helptext})
    #when it is a post
    if request.method == 'POST':
        form = DinamicForm(tuple=forms_to_fill, data=request.POST, files=request.FILES)
        if form.is_valid():
            cleaned_data = form.clean()
            form.save(request.user, activity, cleaned_data)
            if old_data:
                messages.success(request, "A sua incrição foi atualizada com sucesso")
            else:
                messages.success(request, "Foi registado com sucesso")
            extra_context['update'] = True
            if activity.public:
                return redirect('activity_participant_list', event.id, activity.id)
            else:
                return redirect('activity_details', event.id, activity.id)
    #populate with old data
    elif old_data:
        form = DinamicForm(tuple=forms_to_fill, data=old_data)
    #else new form
    else:
        form = DinamicForm(tuple=forms_to_fill)
        #extra_context['update'] = False
    template_name = 'userena/activity_register.html'
    #extra stuff
    extra_context['form'] = form
    extra_context['event'] = event
    extra_context['activity'] = activity
    #return render_to_response('userena/register_activity.html', {'formset': formset}, request=request)
    return direct_to_template(request,
                              template_name,
                              extra_context=extra_context)


@login_required
def unregister_activity(request, event_id, activity_id, extra_context=None):
    '''
    Removes a user from an event and deletes all associated data
    '''
    from datetime import timedelta
    #get the event, used more to stop fetching wrong activities
    event = get_object_or_404(Event, pk__iexact=event_id)
    #get the activity
    activity = get_object_or_404(Activity, pk__iexact=activity_id, event__exact=event_id)
    #check for less than 24h quits
    now = datetime.now() + timedelta(days=1) 
    if activity.start_date <= now:
        return direct_to_template(request, '600.html')
    #delte the user
    get_object_or_404(UserParticipatesInActivity, user=request.user, activity=activity).delete()
    #if there are old records, delete and save the original data
    old = UserParticipatesInActivityMetadata.objects.filter(user=request.user, activity=activity)
    if old.count() > 0:
        old.delete()
    messages.success(request, "A sua incrição foi removida")
    return redirect('activity_details', event.id, activity.id)


class ActivityDetailView(DetailView):
    '''shows sigle activity details'''
    context_object_name = "activity"
    template_name = "userena/activity_details.html"
    model = Activity
    sluf_field = "id"
    
    def get_object(self):
        '''the object to be showed'''
        return get_object_or_404(Activity, pk__exact=self.kwargs['activity_id'], event__exact=self.kwargs['event_id'])

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(ActivityDetailView, self).get_context_data(**kwargs)
        #get the event, used more to stop fetching wrong activities
        event = get_object_or_404(Event, pk__iexact=self.kwargs['event_id'])
        #get the activity
        activity = get_object_or_404(Activity, pk__iexact=self.kwargs['activity_id'], event__exact=self.kwargs['event_id'])
        try:
            UserParticipatesInActivity.objects.get(user=self.request.user, activity=activity)
            context['update'] = True
        except:
            context['update'] = False
        #extra info
        if activity.extra_info != '':
            context['extra_info'] = self.split_lines(activity.extra_info)
        else:
            context['extra_info'] = False
        context['event'] = event
        context['activity'] = activity
        #files
        context['files'] = activity.attachments.all()
        context['doc_url'] = settings.ACTIVITY_DOC_URL
        return context

    def split_lines(self, text):
        '''splits extra_info in lines for the template'''
        spl = []
        for l in text.split('\n'):
            spl.append(l)
        return spl


class EventListView(ListView):
    model = Event
    template_name = 'userena/event_list.html'
    context_object_name = "event_list"
    #paginator = Paginator(Event, 1)
    paginate_by = 5
    
    def get_queryset(self):
        return Event.object.all().order_by('-start_date')


class ParticipantListView(ListView):
    '''list view for participants in an activity'''
    template_name = 'userena/activity_participant_list.html'
    context_object_name = "participant_list"
    paginate_by = 100

    def get_queryset(self):
        '''dummy query, has to be here'''
        profile_model = get_profile_model()
        return profile_model.objects.none()

    def get_context_data(self, **kwargs):
        '''set context and extra variables for template engine'''
        # Call the base implementation first to get a context
        context = super(ParticipantListView, self).get_context_data(**kwargs)
        #get the event, used more to stop fetching wrong activities
        event = get_object_or_404(Event, pk__iexact=self.kwargs['event_id'])
        #get the activity
        activity = get_object_or_404(Activity, pk__iexact=self.kwargs['activity_id'], event__exact=self.kwargs['event_id'])
        #get enrolled users
        users = UserParticipatesInActivity.objects.filter(activity=activity).values_list('user', flat=True)
        current_users = User.objects.filter(pk__in=users)
        allusers = current_users.values_list('username', flat=True)
        #compare with viewable profiles
        profile_model = get_profile_model()
        profiles = profile_model.objects.get_visible_profiles(self.request.user).filter(user__in=current_users)
        #make a list for the view
        participant_list = []
        #list gets the full names or login names
        #first element the full name, second the profile name/slug for linking
        for o in profiles:
            temp_list = []
            temp_list.append(o.get_full_name_or_username())
            temp_list.append(o.user.username)
            participant_list.append(temp_list)
        #contructs the user that have private profiles
        if len(participant_list) > 0:
            for u in allusers:
                add = True 
                for p in participant_list:
                    if u in p:
                        add = False
                        break
                if add:
                    participant_list.append([u])
        #else there are no public profiles to show
        else:
            for u in allusers:
                participant_list.append([u])
        context['update'] = True
        #ensures the user is enrolled
        try:
            get_object_or_404(UserParticipatesInActivity, user=self.request.user, activity=activity)
            context['participant_list'] = participant_list
        except:
            context['update'] = False
        context['event'] = event
        context['activity'] = activity
        return context


class ActivityListView(ListView):
    model = Activity
    template_name = 'userena/activity_list.html'
    context_object_name = "activity_list"
    paginate_by = 12

    def get_queryset(self):
        event = get_object_or_404(Event, pk__iexact=self.kwargs['event_id'])
        return Activity.objects.filter(event=event).order_by('start_date')
    
    def get_context_data(self, **kwargs):
        '''set context and extra variables for template engine'''
        # Call the base implementation first to get a context
        context = super(ActivityListView, self).get_context_data(**kwargs)
        #get the event, used more to stop fetching wrong activities
        event = get_object_or_404(Event, pk__iexact=self.kwargs['event_id'])
        context['event'] = event
        return context
