# -*- coding: utf-8 -*-
from django.forms import ModelForm
from event.models import UserParticipatesInActivityMetadata, Attribute,\
    UserParticipatesInActivity
from django import forms
from django.forms.fields import CharField, FileField, BooleanField,\
    ChoiceField, FloatField
from django.forms.widgets import Textarea
from django.db import transaction
from django.utils.datetime_safe import datetime
from django.core.exceptions import ObjectDoesNotExist


class DinamicForm(forms.Form):
    def __init__(self, tuple, *args, **kwargs):
        super(DinamicForm, self).__init__(*args, **kwargs)
        for item in tuple:
            #text
            if item['type'] == 'text':
                self.fields[item['name']] = CharField()
            #files
            elif item['type'] == 'file':
                self.fields[item['name']] = FileField()
            #numbers
            elif item['type'] == 'number':
                self.fields[item['name']] = FloatField()
            #text areas
            elif item['type'] == 'textarea':
                self.fields[item['name']] = CharField(widget=Textarea())
            #boolean fields
            elif item['type'] == 'boolean':
                self.fields[item['name']] = BooleanField()
            #select fields
            elif item['type'] == 'dropdown':
                choices = []
                ch = unicode(item['extra']).split(',')
                #get the choices in the required arrays for the widget
                for x in ch:
                    choices.append([x, x])
                self.fields[item['name']] = ChoiceField(choices=choices)
            #check for mandatory
            if item['mandatory']:
                self.fields[item['name']].required = True
            else:
                self.fields[item['name']].required = False
            #help text para os campos
            if item['help'] != '':
                self.fields[item['name']].help_text = item['help']

    def clean(self):
        self.cleaned_data = super(DinamicForm, self).clean()
        mandatorymsg = u"Este campo é obrigatório"
        for k in self.fields:
            #check for boolean fields and set it to false in case it wasn't selected
            if isinstance(self.fields[k], BooleanField):
                if k not in self.cleaned_data:
                    self.cleaned_data[k] = False
                    del self.errors[k]
            #required attributes, acho que agora nao e preciso fazer esta verificarção, o super já faz
            #no entanto fica aqui, just in case
            if self.fields[k].required == True:
                if k not in self.cleaned_data:
                    self._errors[k] = self.error_class([mandatorymsg])
                elif self.cleaned_data[k] == '':
                    self._errors[k] = self.error_class([mandatorymsg])
        return self.cleaned_data

    @transaction.commit_on_success
    def save(self, user, activity, cleaned_data, now = datetime.now()):
        '''
        saves the form on the database
        TODO: SAVE THE FILES!!!!
        '''
        try:
            UserParticipatesInActivity.objects.get(user=user, activity=activity)
        except ObjectDoesNotExist:
            UserParticipatesInActivity(user=user, activity=activity, status=activity.default_status).save()
        #if there are old records, delete and save the original data
        old = UserParticipatesInActivityMetadata.objects.filter(user=user, activity=activity)
        if old.count() > 0:
            now = old[0].enrollment_date
            old.delete()
        #inserts new data
        for data in cleaned_data:
            if cleaned_data[data] is None:
                continue
            if cleaned_data[data] == '':
                continue
            attribute = Attribute.objects.get(name=unicode(data))
            UserParticipatesInActivityMetadata(user=user, activity=activity, attribute=attribute, value=unicode(cleaned_data[data]), enrollment_date=now).save()
