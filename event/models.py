from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.conf import settings
from event.managers import Future_events, AllEvents
from django.utils.datetime_safe import datetime
import os.path
from document.models import Document


class Future_events(models.Manager):
    def get_query_set(self):
        return super(Future_events, self).get_query_set().filter(end_date__gt=datetime.now())


class Attribute(models.Model):
    '''defines the attributes of the material'''
    CHOICES = (('text', 'text'), ('number', 'number'), ('file', 'file'), ('textarea', 'textarea'),
               ('boolean', 'boolean'), ('dropdown', 'dropdown'),)

    name = models.CharField('form field', max_length=100, unique=True)
    type = models.CharField('Type of field', choices=CHOICES, max_length=10)

    def __unicode__(self):
        str_list = []
        str_list.append(self.name)
        str_list.append(':')
        str_list.append(self.type)
        return ''.join(str_list)

    class Meta:
        verbose_name = 'Activity form field'
        verbose_name_plural = 'Activity form fields'


class Event(models.Model):
    '''
    general event
    '''
    name = models.CharField(max_length=100, verbose_name="name", unique=True)
    description = models.TextField(max_length=500, verbose_name="description")
    url = models.URLField(max_length=100, verbose_name="website", blank=True, null=True)
    logo = models.FileField(upload_to=settings.MEDIA_ROOT, verbose_name="event logo/image", blank=True, null=True)
    start_date = models.DateTimeField('start date')
    end_date = models.DateTimeField('end date')

    #managers
    object = AllEvents()
    future_events = Future_events()

    @property
    def get_start_date_day_month(self):
        '''returns active events'''
        return self.start_date

    @property
    def get_logo(self):
        '''returns logo relative path'''
        return os.path.basename(self.logo.name)

    @property
    def get_description_limited(self):
        '''returns active events'''
        return self.description[:100]

    def clean(self):
        '''
        validate the models
        '''
        super(Event, self).clean()
        if self.end_date < self.start_date:
            raise ValidationError('Start date must be after end date')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = 'Event'
        verbose_name_plural = 'Events'
        ordering = ["end_date"]


class Activity (models.Model):
    '''
    an activity of the event
    can be enrolled by various users
    '''
    status_choices = (('enrolled', 'enrolled'), ('pending', 'pending'), ('canceled', 'canceled'),)

    name = models.CharField(max_length=100, verbose_name="name")
    description = models.TextField(max_length=500, verbose_name="description")
    extra_info = models.TextField(max_length=500, verbose_name="extra info for participants", blank=True, null=True)
    public = models.BooleanField(default=False, verbose_name="participant list is public")
    default_status = models.CharField(max_length=10, verbose_name='default status for new enrollments', choices=status_choices, default='enrolled')
    start_date = models.DateTimeField('start date')
    end_date = models.DateTimeField('end date')
    fine_location = models.CharField(max_length=100, verbose_name="location")
    map_location = models.CharField(max_length=100, verbose_name="map location", blank=True, null=True)
    event = models.ForeignKey(Event, verbose_name="event")
    participants_metadata = models.ManyToManyField(User, through='UserParticipatesInActivityMetadata', related_name='usermetadata', blank=True, null=True)
    participants = models.ManyToManyField(User, through='UserParticipatesInActivity', blank=True, null=True, related_name='userparticipates')
    url = models.URLField(max_length=100, verbose_name="website", blank=True, null=True)
    attributes = models.ManyToManyField(Attribute, through='ActivityHasAttributes', blank=True, null=True, verbose_name='activity extra fields')
    max_slots = models.IntegerField(verbose_name="Maximum number of participants", blank=True, null=True)
    attachments = models.ManyToManyField(Document, blank=True, null=True, verbose_name='File attachments')

    @property
    def get_number_of_open_slots(self):
        '''return number of open slots, -1 if there is no limit'''
        #if no limit, return None
        if not self.max_slots:
            return -1
        else:
            slots = UserParticipatesInActivity.objects.filter(activity=self.pk).count()
            return self.max_slots - slots

    @property
    def is_open(self):
        '''return a boolean if its still in a valid time'''
        if self.start_date > datetime.now():
            return True
        else:
            return False

    '''admin buttons'''
    def view_participants_button(self):
        return '<a href="/admin/event/activity/{0}/viewparticipants/" class="add-another" id="add_id_element"><img src="{1}admin/img/admin/selector-search.gif" width="10" height="10" alt="Show participants"> Mostrar inscritos</a>'.format(self.pk, settings.STATIC_URL)
    view_participants_button.allow_tags = True
    view_participants_button.short_description = 'Mostrar participantes'

    def __unicode__(self):
        return self.name

    def clean(self):
        '''
        validate the models
        '''
        super(Activity, self).clean()
        #see if theres a event selected
        try:
            parent_event = self.event
        except ObjectDoesNotExist:
            raise ValidationError('Please chose an event')
        #check for invalid numbers in participants
        if self.max_slots is not None:
            if self.max_slots < 1:
                raise ValidationError('Maximum number of participants must be bigger than 1 or nothing')
        #verify if the activity dates are between the event dates
        if self.start_date < parent_event.start_date or self.start_date > parent_event.end_date:
            raise ValidationError('Invalid start date, must be between {0} and {1}'.format(parent_event.start_date, parent_event.end_date))
        if self.end_date < parent_event.start_date or self.end_date > parent_event.end_date:
            raise ValidationError('Invalid end date, must be between {0} and {1}'.format(parent_event.start_date, parent_event.end_date))
        if self.end_date < self.start_date:
            raise ValidationError('Start date must be after end date')

    class Meta:
        verbose_name = 'Activity'
        verbose_name_plural = 'Activities'
        unique_together = ('name', 'event')


class ActivityHasAttributes(models.Model):
    '''
    NxN relationship table
    has a boolean to dictate mandatory fields
    '''
    types_that_have_extra = ('dropdown')

    attribute = models.ForeignKey(Attribute)
    activity = models.ForeignKey(Activity)
    mandatory = models.BooleanField(verbose_name='field is mandatory', default=True)
    helptext = models.CharField(verbose_name='help text', max_length=200, blank=True, null=True)
    extra = models.CharField('extra information', max_length=100, blank=True, null=True)

    @property
    def get_type(self):
        return 'nada'

    def clean(self):
        '''
        validate the models
        '''
        super(ActivityHasAttributes, self).clean()
        if self.attribute:
            type = self.attribute.type
            #check for extra fiels only on allowed types
            if type == 'dropdown' and self.extra == '':
                raise ValidationError('Type {0} needs extra information, at least 2 fields separated by commas'.format(type))
            if self.extra != '':
                if type not in self.types_that_have_extra:
                    raise ValidationError('Type {0} cannot have extra information'.format(type))
                if type == 'dropdown':
                    if unicode(self.extra).count(',') < 2:
                        raise ValidationError('you have to have at least 2 options, separated by commas')

    def __unicode__(self):
        str_list = []
        str_list.append(self.attribute.name)
        str_list.append(':')
        str_list.append(self.activity.name)
        str_list.append('=')
        str_list.append(str(self.mandatory))
        return ''.join(str_list)

    class Meta:
        verbose_name = 'Activity field'
        verbose_name_plural = 'Activity fields'
        unique_together = ('attribute', 'activity')


class UserParticipatesInActivity(models.Model):
    '''
    NXN table relationship that saves the date subscribes to an activity
    could be inferred by the other table, but this is the primary table to see if a user
    participates in a event and good to handle events without metadata
    '''
    status_choices = (('enrolled', 'enrolled'), ('pending', 'pending'), ('canceled', 'canceled'),)
    user = models.ForeignKey(User)
    activity = models.ForeignKey(Activity)
    enrollment_date = models.DateTimeField(verbose_name="enrollment date")
    status = models.CharField(max_length=10, verbose_name='enrollment status', choices=status_choices)

    def __unicode__(self):
        str_list = []
        str_list.append(self.activity.name)
        str_list.append(' subscribed to ')
        str_list.append(self.user.__unicode__())
        str_list.append(' on ')
        str_list.append(unicode(self.enrollment_date))
        return ''.join(str_list)

    class Meta:
        verbose_name = 'User participates in activity'
        verbose_name_plural = 'User participates in activity'
        unique_together = ('user', 'activity')

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id and not self.enrollment_date:
            self.enrollment_date = datetime.now()
        super(UserParticipatesInActivity, self).save(*args, **kwargs)


class UserParticipatesInActivityMetadata(models.Model):
    '''
    NXN table relationship that saves the data and info a user subscribes to an activity
    '''
    user = models.ForeignKey(User)
    activity = models.ForeignKey(Activity)
    attribute = models.ForeignKey(Attribute)
    enrollment_date = models.DateTimeField(verbose_name="enrollment date")
    change_date = models.DateTimeField()
    value = models.CharField('value', max_length=1000)

    def __unicode__(self):
        str_list = []
        str_list.append(self.user.__unicode__())
        str_list.append(':')
        str_list.append(self.activity.name)
        str_list.append(':')
        str_list.append(self.attribute.__unicode__())
        str_list.append(':')
        str_list.append(unicode(self.enrollment_date))
        str_list.append('=')
        str_list.append(self.value)
        return ''.join(str_list)

    class Meta:
        verbose_name = 'User metadata on participation on activity'
        verbose_name_plural = 'User metadata on participation on activities'
        unique_together = ('user', 'activity', 'attribute')

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id and not self.enrollment_date:
            self.enrollment_date = datetime.now()
        self.change_date = datetime.now()
        super(UserParticipatesInActivityMetadata, self).save(*args, **kwargs)
