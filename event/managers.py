from django.db import models
from django.utils.datetime_safe import datetime


class Future_events(models.Manager):
    '''manager that returns events that end after now()'''
    def get_query_set(self):
        return super(Future_events, self).get_query_set().filter(end_date__gt=datetime.now())


class AllEvents(models.Manager):
    '''manager that returns events that end after now()'''
    def get_query_set(self):
        return super(AllEvents, self).get_query_set().all()