from django.conf.urls.defaults import *
from django.views.generic.simple import direct_to_template
from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import login_required

from userena import views as userena_views
from event import views as event_views
from django.views.generic.list import ListView
from event.models import Event

urlpatterns = patterns('',
    url(r'^$',
        login_required(event_views.EventListView.as_view())),
    url(r'^(?P<event_id>\d+)/$',
        login_required(event_views.ActivityListView.as_view())),
    url(r'^(?P<event_id>\d+)/(?P<activity_id>\d+)/enroll/$',
        event_views.register_activity,
        name='activity_enroll'),
    url(r'^(?P<event_id>\d+)/(?P<activity_id>\d+)/delete/$',
        event_views.unregister_activity,
        name='unregister_activity'),
    url(r'^(?P<event_id>\d+)/(?P<activity_id>\d+)/$',
        login_required(event_views.ActivityDetailView.as_view()),
        name='activity_details'),
    url(r'^(?P<event_id>\d+)/(?P<activity_id>\d+)/view/$',
        login_required(event_views.ParticipantListView.as_view()),
        name='activity_participant_list'),
)
