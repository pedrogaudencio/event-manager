import os, sys

'''
1 - bull
2 - neei
'''
CURRENT_MACHINE = 1

abspath = lambda * p: os.path.abspath(os.path.join(*p))

PROJECT_ROOT = abspath(os.path.dirname(__file__))
USERENA_MODULE_PATH = abspath(PROJECT_ROOT, '..')
sys.path.insert(0, USERENA_MODULE_PATH)

DEBUG = True
TEMPLATE_DEBUG = DEBUG

if CURRENT_MACHINE == 1:
    STATIC_ROOT = '/home/joaoascenso/git/aauecurriculos/static/'
    STATIC_URL = 'http://127.0.0.1:8000/static/'
elif CURRENT_MACHINE == 2:
    STATIC_ROOT = '/home/joaoascenso/git/aauecurriculos/static/'
    STATIC_URL = 'http://eventos.aaue.pt/static/'

ADMINS = (
    # ('Your Name', 'your_email@domain.com'),
)

MANAGERS = ADMINS

if CURRENT_MACHINE == 1:
    DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'aaueeventos', # Or path to database file if using sqlite3.
        'USER': 'aaueeventos', # Not used with sqlite3.
        'PASSWORD': 'aaueeventos', # Not used with sqlite3.
        'HOST': '', # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '', # Set to empty string for default. Not used with sqlite3.
        }
    }

elif CURRENT_MACHINE == 2:
    DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'aaueeventos', # Or path to database file if using sqlite3.
        'USER': 'aaueeventos', # Not used with sqlite3.
        'PASSWORD': 'zSAY8cZuSyvx74xZ', # Not used with sqlite3.
        'HOST': '', # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '', # Set to empty string for default. Not used with sqlite3.
        }
    }

if DEBUG:
    # Use the Python SMTP debugging server. You can run it with:
    # ``python -m smtpd -n -c DebuggingServer localhost:1025``.
    EMAIL_PORT = 1025
else:
    EMAIL_PORT = 25

if CURRENT_MACHINE == 2:
    EMAIL_PORT = 25

TIME_ZONE = 'Europe/Lisbon'
LANGUAGE_CODE = 'pt-br'

ugettext = lambda s: s
LANGUAGES = (
    ('pt', ugettext('Portuguese')),
)

SITE_ID = 1

USE_I18N = True
USE_L10N = True

MEDIA_ROOT = abspath(PROJECT_ROOT, 'media')
DOCUMENT_ROOT = abspath(PROJECT_ROOT, 'docs')

MEDIA_URL = '/media/'

ACTIVITY_DOC_ROOT = MEDIA_ROOT + '/activity'
ACTIVITY_DOC_URL = MEDIA_URL + 'activity/'

ADMIN_MEDIA_PREFIX = '/static/admin/'

SECRET_KEY = 'sx405#tc)5m@s#^jh5l7$k#cl3ekg)jtbo2ds(n(kw@gp0t7x@'

STATICFILES_DIRS = (
    abspath(PROJECT_ROOT, 'static'),
    abspath(PROJECT_ROOT, 'media/css'),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)


TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'userena.middleware.UserenaLocaleMiddleware',
)
TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.contrib.messages.context_processors.messages",
    "django.core.context_processors.request",
)

AUTHENTICATION_BACKENDS = (
    'userena.backends.UserenaAuthenticationBackend',
    'guardian.backends.ObjectPermissionBackend',
    'django.contrib.auth.backends.ModelBackend',
)

ROOT_URLCONF = 'urls'

TEMPLATE_DIRS = (
    abspath(PROJECT_ROOT, 'templates')
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.admin',
    'django.contrib.staticfiles',
    'easy_thumbnails',
    'guardian',
    'south',
    'userena',
    'contrib.umessages',
    'profiles',
    'devserver',
    'easy_maps',
    #gestao de eventos:
    'event',
    'document',

)

# Userena settings
LOGIN_REDIRECT_URL = '/accounts/%(username)s/'
LOGIN_URL = '/accounts/signin/'
LOGOUT_URL = '/accounts/signout/'
AUTH_PROFILE_MODULE = 'profiles.Profile'

USERENA_ACTIVATION_DAYS = 7

USERENA_DISABLE_PROFILE_LIST = False
USERENA_MUGSHOT_SIZE = 140

# Test settings
TEST_RUNNER = 'django.test.simple.DjangoTestSuiteRunner'
SOUTH_TESTS_MIGRATE = False

# Guardian
ANONYMOUS_USER_ID = -1
#maps
EASY_MAPS_GOOGLE_KEY = "AIzaSyAFma0RK32pkpi8_1w63QJQDAPEfhXZ4Tk"

DEFAULT_FROM_EMAIL = "geral@aaue.pt"

#USERENA_MUGSHOT_PATH = STATIC_ROOT
